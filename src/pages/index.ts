import ContactUs from "./ContactUs/ContactUs";
import Home from "./Home/Home";
import Menu from "./Menu/Menu";
import NotFoundPage from "./NotFoundPage/NotFoundPage";

export { ContactUs, Home, Menu, NotFoundPage };
