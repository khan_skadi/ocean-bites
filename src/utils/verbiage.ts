export const ALERT_MESSAGES = {
  formSubmittedSuccessfully: "Thank you for contacting us, we'll get back to you soon!",
  errorSubmittingForm: "There was an error submitting the form. Please try again later.",
};
